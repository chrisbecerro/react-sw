function Componente1() {
  return (
    <>
      <div className="title-menu-c1">
        <h1>ALUMNO</h1>
      </div>
      <div className="container-c1">
        <div className="title">
          <h1>Datos</h1>
        </div>
        <div className="img-c1">
          <img className="img-c" src="/src/assets/elchristo.jpeg" alt="Dia feliz" />
        </div>
        <div className="date-c1">
          <h5>Nombre: Christopher Becerro Bollera</h5>
        </div>
        <div className="elementary-c1">
          <ul>
            <li>Carrera: ISC</li>
            <li>Materia: SW</li>
            <li>Docente: Memotl</li>
          </ul>
        </div>
      </div>
      <div className="title-menu-c1">
        <h1>GUSTOS</h1>
      </div>
    </>
  )
}
export default Componente1