import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'
import Componente1 from './components/Componente1'
import Componente2 from './components/Componente2'
import Componente3 from './components/Componente3'
import Componente4 from './components/Componente4'
import Componente5 from './components/Componente5'
import Componente6 from './components/Componente6'

function App() {
  // const [count, setCount] = useState(0)

  return (
    <>
    <Componente1/>
    <Componente2/>
    <Componente3/>
    <Componente4/>
    <Componente5/>
    <Componente6/>
    </>
  )
}
export default App
